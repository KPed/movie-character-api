# Movie Character API

Movie Character API is a Web API made using JPA (Hibernate), which you can query for Movie, Franchise and Character data. 

It is a submission for an assignment from the Noroff Accelerate course.


## Installation

### Heroku

It should be accessible on Heroku with no setup required at this url: [heroku-hosted-api](https://movie-character-db.herokuapp.com/swagger-ui/index.html)

### Local

Requirements:
- Docker

Follow these steps to get it running locally: 

1. Download the project to a local folder (clone, fork or download as zip)

2. Navigate terminal to project root folder

3. Run the command ``docker-compose up``

4. The application should now be running at http://localhost:8080


## Usage

Once you have the app running (either locally or through Heroku) you can add "/swagger-ui.html"  to the root URL of your application to see the available API endpoints. It should look like this: localhost:8080/swagger-ui.html

You can then make requests through the Swagger UI.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Maintainers
- @KPed
- @Noitcereon

## License
[MIT](https://choosealicense.com/licenses/mit/)
