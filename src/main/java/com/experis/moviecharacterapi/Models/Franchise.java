package com.experis.moviecharacterapi.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Franchise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String name;

    @Column(length = 750)
    public String description;

    @JsonGetter(value = "movies")
    public List<String> getMovies() {
        if(movies == null) return null;

        //Return api path to movie
        return movies.stream().map(movie -> {
            return String.format("/api/movie/%d", movie.id);
        }).collect(Collectors.toList());
    }

    @OneToMany(mappedBy = "franchise", fetch = FetchType.LAZY)
    public List<Movie> movies;
}
