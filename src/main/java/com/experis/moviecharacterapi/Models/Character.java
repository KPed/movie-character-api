package com.experis.moviecharacterapi.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String fullName;

    @Column
    public String alias;

    @Column
    public String gender;

    @Column
    public String imageUrl;

    @JsonGetter(value = "movies")
    public List<String> getMovies() {
        if(movies == null) return null;

        //Return api path to movie
        return movies.stream().map(movie -> {
            return String.format("/api/movie/%d", movie.id);
        }).collect(Collectors.toList());
    }

    @ManyToMany(mappedBy = "characters", fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    public List<Movie> movies = new ArrayList<>();
}
