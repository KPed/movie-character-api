package com.experis.moviecharacterapi.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String title;

    @Column
    public String genre;

    @Column
    public Integer releaseYear;

    @Column
    public String director;

    @Column
    public String imageUrl; //url to image of a movie poster

    @Column
    public String trailerUrl; //most likely a youtube url

    @JsonGetter(value = "franchise")
    public String getFranchise(){
        if(franchise == null) return null;
        return String.format("/api/franchise/%d", franchise.id);
    }

    @ManyToOne
    @JoinColumn(name = "fk_franchise_id")
    public Franchise franchise;

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    @JsonGetter(value = "characters")
    public List<String> getCharacters(){
        if(characters == null) return null;

        //Return api path to character
        return characters.stream().map(character -> {
            return String.format("/api/character/%d", character.id);
        }).collect(Collectors.toList());
    }

    @ManyToMany(fetch = FetchType.LAZY)
    public List<Character> characters = new ArrayList<>();

}
