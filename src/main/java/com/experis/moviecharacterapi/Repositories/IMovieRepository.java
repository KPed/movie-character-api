package com.experis.moviecharacterapi.Repositories;

import com.experis.moviecharacterapi.Models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IMovieRepository extends JpaRepository<Movie, Integer> {
}
