package com.experis.moviecharacterapi.Controllers;

import com.experis.moviecharacterapi.Models.Character;
import com.experis.moviecharacterapi.Repositories.ICharacterRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api")
public class CharacterController {

    private final ICharacterRepository characterRepository;

    public CharacterController(ICharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @GetMapping("/character")
    public ResponseEntity<List<Character>> getAllCharacters() {
        List<Character> characterData = characterRepository.findAll();
        return ResponseEntity.ok(characterData);
    }

    @GetMapping("/character/{characterId}")
    public ResponseEntity<Character> findCharacterById(@PathVariable Integer characterId) {
        Optional<Character> searchResult = characterRepository.findById(characterId);
        if (searchResult.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(searchResult.get());
    }

    @PostMapping("/character")
    public ResponseEntity<Character> createCharacter(@RequestBody Character character) {
        Character createdCharacter = characterRepository.save(character);
        return ResponseEntity.ok(createdCharacter);
    }

    @PutMapping("/character/{characterId}")
    public  ResponseEntity<Character> updateCharacter(@RequestBody Character characterUpdate, @PathVariable Integer characterId){
        if (!characterRepository.existsById(characterId)){
            return ResponseEntity.notFound().build();
        }
        Character character = characterRepository.findById(characterId).get();

        character.fullName = characterUpdate.fullName;
        character.alias = characterUpdate.alias;
        character.gender = characterUpdate.gender;
        character.imageUrl = characterUpdate.imageUrl;

        characterRepository.save(character);
        return ResponseEntity.ok(character);
    }
    @DeleteMapping("character/{characterId}")
    public ResponseEntity<Boolean> deleteCharacterById(@PathVariable Integer characterId){
        characterRepository.deleteById(characterId);
        return ResponseEntity.ok(true); // if this is reached it should've been a success
    }
}
